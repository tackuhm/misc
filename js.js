// jsだとyield（ジェネレータ）まわりが弱いのであまりやる意味ないかも
// fizzbuzzがアロー関数っていう縛りが無いほうが問題設定としてよかったかも

const fizzbuzz = (arg) => {
  const _fizzbuzz = (i) => {
    return i % 15 === 0
      ? "FizzBuzz"
      : i % 5 === 0
      ? "Buzz"
      : i % 3 === 0
      ? "Fizz"
      : i;
  };

  return (function* () {
    for (const i of [...Array(arg).keys()]) {
      yield _fizzbuzz(i);
    }
  })();
};

for (const i of fizzbuzz(20)) {
  console.log(i);
}
