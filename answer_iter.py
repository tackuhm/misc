# 一番キモい
# 'STOP'というstrで判定してるのが最もキモい

def fizzbuzz(arg):
  class FizzBuzzer:
    def __init__(self, max):
      self.i = 0
      self.max = max

    def __call__(self):
      if (self.i == self.max):
        return 'STOP'
      self.i += 1
      return self.calc(self.i - 1)

    def calc(self, i):
      if (i%15 == 0):
        return 'FizzBuzz'
      elif (i%5 == 0):
        return 'Buzz'
      elif (i%3 == 0):
        return 'Fizz'
      else:
        return i


  return iter(FizzBuzzer(arg), 'STOP')

for i in fizzbuzz(20):
  print(i)