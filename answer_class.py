# キモい

def fizzbuzz(arg):
  class FizzBuzzer:
    def __init__(self, max):
      self.i = 0
      self.max = max

    def __iter__(self):
      return self

    def __next__(self):
      if self.i == self.max:
        raise StopIteration()
      self.i += 1
      return self.calc(self.i - 1)

    def calc(self, i):
      if (i%15 == 0):
        return 'FizzBuzz'
      elif (i%5 == 0):
        return 'Buzz'
      elif (i%3 == 0):
        return 'Fizz'
      else:
        return i
  
  return FizzBuzzer(arg)

for i in fizzbuzz(20):
  print(i)