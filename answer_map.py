# ループロジックと値判定ロジックは分けつつmapでやってみるマン
# for内包とジェネレータ内包とmapはここでは等価なので全パターンはやりません

def fizzbuzz(arg):
  def calc(i):
    if (i%15 == 0):
      return 'FizzBuzz'
    elif (i%5 == 0):
      return 'Buzz'
    elif (i%3 == 0):
      return 'Fizz'
    else:
      return i

  return map(calc, range(arg))

for i in fizzbuzz(20):
  print(i)