# iter関数が__getitem__に反応することを利用。
# わざわざやる意味はないけど

def fizzbuzz(arg):
  class FizzBuzzer:
    def __init__(self, max):
      self.max = max

    def __getitem__(self, i):
      if (i == self.max):
        raise IndexError()

      if (i%15 == 0):
        return 'FizzBuzz'
      elif (i%5 == 0):
        return 'Buzz'
      elif (i%3 == 0):
        return 'Fizz'
      else:
        return i

  return iter(FizzBuzzer(arg))

for i in fizzbuzz(20):
  print(i)