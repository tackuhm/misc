# ループロジックと値判定ロジックは分けつつ行数少ない方がいいマン

def fizzbuzz(arg):
  def inner(i):
    return 'FizzBuzz' if (i%15 == 0) else 'Buzz' if (i%5 == 0) else 'Fizz' if (i%3 == 0) else i

  return [inner(i) for i in range(arg)]

for i in fizzbuzz(20):
  print(i)