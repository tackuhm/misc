# これが一番現実的だと思っている

def fizzbuzz(arg):
  for i in range(arg):
    if (i%15 == 0):
      yield 'FizzBuzz'
    elif (i%5 == 0):
      yield 'Buzz'
    elif (i%3 == 0):
      yield 'Fizz'
    else:
      yield i

for i in fizzbuzz(20):
  print(i)