以下のファイルの指定箇所を改変して、後述の実行結果になるようにしてください。

```python
# fizzbuzz.py
def fizzbuzz(arg):
  # ★★★ ここを変える ★★★

for i in fizzbuzz(20):
  print(i)
```

```
$ python fizzbuzz.py
FizzBuzz
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
```
